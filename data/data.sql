---权限控制
CREATE TABLE `os_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `account` varchar(255) NOT NULL DEFAULT '' COMMENT '登录账号',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '登录密码',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1正常，2锁定',
  `token` char(22) not null default '' comment '登录凭证',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `index_account` (`account`),
  KEY `index_token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='后台账号表';

CREATE TABLE `os_admin_role_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `admin_id` int(11) not null default 0 COMMENT '账号id ,对应表中的id，不一定是admin_id,可能是member_id',
  `role_id` int(11) not null DEFAULT 0 comment '角色id',
  `type` enum('ADMIN','MEMBER') not null default 'ADMIN' COMMENT '账号类型，enum值基本和表名一致',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `index_admin_id` (`admin_id`),
  KEY `index_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='账号角色关联表';


CREATE TABLE `os_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组id,自增主键',
  `name` char(20) NOT NULL DEFAULT '' COMMENT '角色中文名称',
  `code` char(20) NOT NULL DEFAULT '' COMMENT '角色英文名称，给前端用，supper,clerk,shopowner,hardware，developer',
  `description` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='权限组';


CREATE TABLE `os_access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限id,自增主键',
  `name` char(20) NOT NULL DEFAULT '' COMMENT '权限名称',
  `route` char(100) NOT NULL DEFAULT '' COMMENT '权限路由，相当于模块控制器方法，admin/index/index',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `index_route` (`route`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='访问权限';


CREATE TABLE `os_role_access_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int(11) not null default 0 COMMENT '角色,对应表中的id，不一定是admin_id,可能是member_id',
  `access_id` int(11) not null DEFAULT 0 comment '权限id',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `index_role_id` (`role_id`),
  KEY `index_access_id` (`access_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='账号角色关联表';

--部门表
CREATE TABLE `os_department`(
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) notl null comment '部门名称',
  `charge_id` int(11) comment '部门负责人，关联用户表',
  `canteen_id` int(11)　DEFAULT 0 comment '关联饭堂id',
  `create_time` datetime comment '创建时间',
  PRIMARY KEY (`id`),
  key `charge_id` (`charge_id`),
  key `canteen_id` (`canteen_id`)
)engine=innodb default charset = utf8 comment '部门表';

-- 部门角色表,普工，部门管理员
CREATE TABLE `os_department_role`(
  `id` int(11) not null auto_increment,
  `name` varchar(50) comment '角色名称',
  `role_code` varchar(50) comment '角色代码,general:普通员工;charge:部门负责人;admin:部门综合管理人员',
  `dep_id` int(11) comment '关联的部门id',
  `create_time` datetime comment '创建时间',
  PRIMARY KEY (`id`),
  key `dep_role_code` (`role_code`),
  key `dep_id` (`dep_id`)
)engine=innodb default charset=utf8 comment '部门角色表';

--- 食堂
create table `os_canteen` (
  `id` int(11) not null auto_increment,
  `name` VARCHAR(50) comment '饭堂名称',
  `charge_openid` int(11) comment '饭堂管理员,关联用户表id',
  `chief_openid` int(11) comment '厨师长,关联用户表openid',
  `position` VARCHAR(255) comment '食堂所在的位置',
  `creat_time` datetime comment '创建时间'
  PRIMARY KEY (`id`),
  key `charge_openid` (`charge_openid`),
  key `chief_openid` (`chief_openid`),
)engine=innodb DEFAULT charset=utf8 comment '食堂管理表';

--- 用户表
create table `os_user`(
  `id` int(11) not null auto_increment,
  `openid` VARCHAR(11) not null comment '微信opend_id',
  `nick_name` VARCHAR(50) comment '微信昵称',
  `avator_url` VARCHAR(200) comment '用户头像',
  `real_name` varchar(50) comment '用户真实姓名',
  `birthday` varchar(50) comment '用户生日',
  `mobile` varchar(50) comment '手机号码',
  `age` int(11) comment '年龄',
  `token` char(22) comment '登录token',
  `dep_id` int(11) comment '部门id',
  `dep_role_id` int(11) comment '部门角色id',
  `status` int(5) DEFAULT 0 comment '员工状态,0:初始状态,1:审核通过,２：审核失败',
  `tab_id` int(11) DEFAULT 0 comment '用户标签,审核通过时打上，不同的员工显示不同的菜单',
  `create_time` datetime comment '创建时间',
  `update_time` datetime comment '更新时间',
  PRIMARY key(`id`),
  key `user_dep_id`('dep_id'),
  key `openid`(`openid`),
  key `token`(`token`)
)engine=innodb DEFAULT charset=utf8mb4 comment '用户表';

--- 微信用户标签表
create table `osa_weixin_user_tag`(
  `id` int(11) NOT NULL auto_increment,
  `tag_id` int(11) NOT NULL comment '微信返回的标签id',
  `tag_name` int(11) NOT null comment '标签名称',
  `create_time` datetime NOT NULL comment '添加时间',
  PRIMARY KEY (`id`)
)engine=innodb DEFAULT charset=utf8 comment '微信用户标签';

-- 微信菜单
create table `osa_weixin_menu`(
  `id` int(11) not null auto_increment,
  `pid` int(11) DEFAULT 0 comment '父节点id',
  `name` VARCHAR (50) comment '菜单名称',
  `type` VARCHAR (50) comment '点击后的类型',
  `content` text comment '回复的文字',
  `url` VARCHAR(255) comment '跳转的链接',
  `tag_id` int(11) DEFAULT 0 comment '菜单所属标签,0:默认菜单 ',
  `create_time` datetime comment '创建时间',
  PRIMARY key(`id`),
  key 'tag_id'('tag_id')
)engine=innodb DEFAULT charset=utf8mb4 comment '微信菜单';

-- 食材库分类
CREATE TABLE `osa_ingredients_tag`(
  `id` int(11) NOT NULL auto_increment,
  `name` VARCHAR (50) comment '分类名称',
  `pid` int(11) DEFAULT 0 comment '父级id',
);