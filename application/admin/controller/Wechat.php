<?php
/**
 * Created by PhpStorm.
 * User: guo
 * Date: 18-3-31
 * Time: 下午4:52
 */

namespace app\admin\controller;

use \EasyWeChat\Factory;

class Wechat extends \think\controller
{
    protected $app;

    public function __construct()
    {
        $config = [
            'app_id' => 'wx3cf0f39249eb0exx',
            'secret' => 'f1c242f4f28f735d4687abb469072axx',

            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',

            'log' => [
                'level' => 'debug',
                'file' => __DIR__ . '/wechat.log',
            ],
        ];

        $this->app = Factory::officialAccount($config);

    }

    public function creat_tag()
    {
        $result = $this->app->user_tag->create('你好');

    }
}
